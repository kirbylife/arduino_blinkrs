#![no_std]
#![no_main]

use ruduino::cores::current::port::B5 as led;
use ruduino::delay::delay_ms;
use ruduino::Pin;

#[no_mangle]
pub extern "C" fn main() -> ! {
    led::set_output();

    loop {
        led::set_high();
        delay_ms(1000);
        led::set_low();
        delay_ms(1000);
    }
}
